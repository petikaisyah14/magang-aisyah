<?php

use App\Http\Controllers\MagangController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/pertama', function () {
//     return view('pertama');
// });

// Route::get('/kedua', function () {
//     return view('kedua');
// });

// Route::get('/ketiga', function () {
//     return view('ketiga');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/welcome', "MagangController@Landing");

Route::get('/testing', "MagangController@test");

Route::get('/kedua', "MagangController@dua");


